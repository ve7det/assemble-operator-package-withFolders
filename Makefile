# Usage
# make			# compile document
# make clean	# remove working files
# make spotless	# clean out all, including the output files
# make test		# observe commands with all the variables inserted
# make draft	# just string together the files
# make final	# just convert the strung-together PDF into an ebook version

.PHONY = all clean test spotless draft named

# give the output a name

FILE    = simplex-relay-operator-package

# give the PDF type setting for Ghostscript
# choices: ebook, printer, prepess

DETAIL	= ebook

# define the order

ORDER	= A1 B1 C1 C2 D1 B2 B4 B5 B6 B7 E1 H1

# define the pdf modules

SOURCEA   = VECTOR\ Simplex\ Relay\ Day\ 2021\ Overview.pdf
SOURCEB   = VECTOR\ Simplex\ Relay\ Day\ 2021\ Rules\ and\ Guide.pdf
SOURCEC   = VECTOR\ Simplex\ Relay\ Day\ 2021\ Score\ Sheet\ master.docx.pdf
SOURCED   = VECTOR\ Simplex\ Relay\ Day\ 2021\ -\ Flow\ Diagram.pdf
SOURCEE   = PERCS_Message_Form_Ver1.4.pdf
SOURCEF   = Radiogram-VA3IDJ-colour.pdf
SOURCEG   = RADIOGRAM-2011.pdf
SOURCEH	  = Useful\ links\ master.pdf
#SOURCEI   = Credits\ master.pdf

# filing information

MOD_DIR	= modules
OUT_DIR	= output
SHELL	= /bin/bash

# ACTION:  #####

# Rules for making the "help" list

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

all: pdf draft forms ## Make everything

pdf: final draft named forms  ## Make a PDF

# Shrink the package (Final Target)

final: $(FILE).pdf $(OUT_DIR)/$(FILE)-$(DETAIL).pdf ## Shrink the package to final version

$(OUT_DIR)/$(FILE)-$(DETAIL).pdf: $(FILE).pdf
	@echo "creating ebook";
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.5 -dPDFSETTINGS=/$(DETAIL) -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$(OUT_DIR)/$(FILE)-$(DETAIL).pdf $(FILE).pdf

# Assemble the package (subtarget)

draft: test1.pdf $(MOD_DIR)/$(SOURCEA) $(MOD_DIR)/$(SOURCEB) $(MOD_DIR)/$(SOURCEC) $(MOD_DIR)/$(SOURCED) $(MOD_DIR)/$(SOURCEE) $(MOD_DIR)/$(SOURCEF) $(MOD_DIR)/$(SOURCEG) $(MOD_DIR)/$(SOURCEH)  forms ## Draft the package

test1.pdf: forms
	@echo "assembling...";
	pdftk A=$(MOD_DIR)/$(SOURCEA) B=$(MOD_DIR)/$(SOURCEB) C=$(MOD_DIR)/$(SOURCEC) D=$(MOD_DIR)/$(SOURCED) E=$(MOD_DIR)/$(SOURCEE) F=$(MOD_DIR)/$(SOURCEF) G=$(MOD_DIR)/$(SOURCEG) H=$(MOD_DIR)/$(SOURCEH)  cat $(ORDER) output test1.pdf



# Name the package to short name (subtarget)

named: $(FILE).pdf test1.pdf ## Name package to short name 

$(FILE).pdf: test1.pdf
	cp test1.pdf $(FILE).pdf; cp test1.pdf $(OUT_DIR)/$(FILE).pdf


# Get forms from sources

forms: ARRL RAC RRI PERCS  ## Fetch forms from sources

ARRL: $(MOD_DIR)/RADIOGRAM-2011.pdf

$(MOD_DIR)/RADIOGRAM-2011.pdf:
	wget http://www.arrl.org/files/file/Public%20Service/RADIOGRAM-2011.pdf -O $(MOD_DIR)/RADIOGRAM-2011.pdf

RAC: $(MOD_DIR)/Radiogram-VA3IDJ-colour.pdf

$(MOD_DIR)/Radiogram-VA3IDJ-colour.pdf:
	wget https://www.rac.ca/wp-content/uploads/files/ares//Radiogram-VA3IDJ-colour.pdf -O $(MOD_DIR)/Radiogram-VA3IDJ-colour.pdf

RRI: $(MOD_DIR)/RRI-Radiogram-Form-1602.pdf

$(MOD_DIR)/RRI-Radiogram-Form-1602.pdf:
	wget http://radio-relay.org/wp-content/uploads/2016/11/RRI-Radiogram-Form-1602.pdf -O $(MOD_DIR)/RRI-Radiogram-Form-1602.pdf

# 2020-07-18: PERCS needs to update its Lets-Encrypt Certificate
PERCS: $(MOD_DIR)/PERCS_Message_Form_Ver1.4.pdf

$(MOD_DIR)/PERCS_Message_Form_Ver1.4.pdf:
	wget http://www.percs.bc.ca/wp-content/uploads/2014/06/PERCS_Message_Form_Ver1.4.pdf -O $(MOD_DIR)/PERCS_Message_Form_Ver1.4.pdf

# Check the variable expansions

test:  ## Check the variable expansions
	@echo "===== Assemble Package ==";
	@echo "pdftk A=$(MOD_DIR)/$(SOURCEA) B=$(MOD_DIR)/$(SOURCEB) C=$(MOD_DIR)/$(SOURCEC) D=$(MOD_DIR)/$(SOURCED) E=$(MOD_DIR)/$(SOURCEE) F=$(MOD_DIR)/$(SOURCEF) G=$(MOD_DIR)/$(SOURCEG) H=$(MOD_DIR)/$(SOURCEH) I=$(MOD_DIR)/$(SOURCEI) cat $(ORDER) output test1.pdf"
	@echo "===== Shrink the Package ==";
	@echo "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.5 -dPDFSETTINGS=/$(DETAIL) -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$(OUT_DIR)/$(FILE)-$(DETAIL).pdf $(FILE).pdf";
	@echo "cp test1.pdf $(FILE).pdf; cp test1.pdf $(OUT_DIR)/$(FILE).pdf"

# Clean up the working files

clean: ## Tidy up working files
	rm -vf *.pdf

spotless: ## Wipe spotless the working area
	rm -vf $(OUT_DIR)/*.pdf
	rm -vf *.pdf
	rm -vf $(MOD_DIR)/PERCS_Message_Form_Ver1.4.pdf
	rm -vf $(MOD_DIR)/RADIOGRAM-2011.pdf
	rm -vf $(MOD_DIR)/RadioGgram-VA3IDJ-colour.pdf
	rm -vf $(MOD_DIR)/RRI-Radiogram-Form-1602.pdf

